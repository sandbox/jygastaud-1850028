SMS Nexmo module is a submodule of SMS Framework module.
It allows yourwebsite to send SMS via the Nexmo API.

Use of this module requires a paid account with Nexmo: http://www.nexmo.com
